#!/bin/bash

function baselibraries() {
    apt-get update &&
    apt-get install -y git &&
    apt-get install -y neovim &&
    apt-get install -y curl &&
    apt-get install -y python3 &&
    apt-get install -y make &&
    apt-get install -y gcc &&
    apt-get install -y g++ &&
    apt-get install -y make &&
    apt-get install -y apt-utils &&
    apt-get install -y libkrb5-dev &&
    apt-get install -y librdkafka-dev &&
    apt-get install -y libterm-readline-gnu-perl &&
    apt-get install -y libsasl2-dev &&
    apt-get install -y libssl-dev &&
    apt-get install -y libghc-zlib-dev
}

function extralibraries() {
    exist=$(grep -c 'deb http://security.ubuntu.com/ubuntu bionic-security main' /etc/apt/sources.list)

    if [ $exist == 0 ]; then
        echo '------------------------------------------------------------------------------------------------------'
        echo 'Adding Source'
        echo '------------------------------------------------------------------------------------------------------'
        echo 'deb http://security.ubuntu.com/ubuntu bionic-security main' | sudo tee -a /etc/apt/sources.list
    fi
    apt update && apt-cache policy libssl1.0-dev
    apt-get install -y libssl1.0-dev
}

function ohmyzsh() {
    sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
}

function nvm() {
    curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.39.2/install.sh | bash
}

baselibraries && extralibraries && ohmyzsh && nvm